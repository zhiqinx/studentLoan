package edu.tamucc.studentloan;
//Find icon from http://handyappsforlife.com/images/app-icon/LoanCalculatorIcon.png
import android.app.Activity;

/**
 * Created by zhen on 2/28/16.
 */
public class calculator extends Activity {
    private Double amount;
    private Double rate;
    private Integer year;
    private Double result;

    //constructor
    public calculator(){
        amount =0.0;
        rate=0.0;
        year=5;
        //int choice = Integer.parseInt(((RadioButton) findViewById(R.id.a5years)).getText().toString());
        result=0.0;

    }
    public void setRate(double rate){
        this.rate=rate;
    }
    public void setAmount(double amount){
        this.amount=amount;
    }

    public void setYear(int year){
        this.year=year;
    }

    public Double getAmount(){return amount;}
    public Double getRate(){return rate;}
    public Integer getYear(){return year;}

    //calculate the total
    public void calculateResult(double amount,double rate,int year){
        //result=((amount*(rate/100)*year)+amount)/(year*12);
        result=((amount*(rate/100)*year)+amount)/(year*12);
    }

    //return the value of total
    public double getResult(){
        calculateResult(getAmount(),getRate(),getYear());
        return result;
    }
}
