package edu.tamucc.studentloan;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

    //Model
    private calculator cal;
    //Declare the variable for each usable widget in the APP
    private EditText amount;
    private EditText rate;
    private TextView monthly;
    private TextView selectedYears;
    private RadioGroup year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //create object
        cal = new calculator();
        //Make reference for each widgets
        amount=(EditText) findViewById(R.id.editText2);
        rate=(EditText) findViewById(R.id.editText3);
        monthly=(TextView)findViewById(R.id.calculatedMonthlyTextView);
        selectedYears=(TextView)findViewById(R.id.selectedLengthtextView);
        year=(RadioGroup)findViewById(R.id.radioGroup);

        //Add listener for amount, rate and radio
        amount.addTextChangedListener(amountWatcher);
        rate.addTextChangedListener(rateWatcher);
        year.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
            //    int choice = Integer.parseInt(((RadioButton) findViewById(year.getCheckedRadioButtonId())).getText().toString());
                switch (arg1) {
                    //choice is 5 year
                    case R.id.a5years:
                        //set the text "you select xx years"
                        selectedYears.setText(R.string.selectedLength5textView);
                        //get the value of selected year, and pass the value to setYear
                        cal.setYear(Integer.parseInt(((RadioButton) findViewById(year.getCheckedRadioButtonId())).getText().toString()));
                        //call display method, to calculate the result, and get the final result
                        display();
                        break;
                    case R.id.a10years:
                        selectedYears.setText(R.string.selectedLength10textView);
                        cal.setYear(Integer.parseInt(((RadioButton) findViewById(year.getCheckedRadioButtonId())).getText().toString()));
                        display();
                        break;
                    case R.id.a15years:
                        selectedYears.setText(R.string.selectedLength15textView);
                        cal.setYear(Integer.parseInt(((RadioButton) findViewById(year.getCheckedRadioButtonId())).getText().toString()));
                        display();
                        break;
                    case R.id.a20years:
                        selectedYears.setText(R.string.selectedLength20textView);
                        cal.setYear(Integer.parseInt(((RadioButton) findViewById(year.getCheckedRadioButtonId())).getText().toString()));
                        display();
                        break;
                    case R.id.a25years:
                        selectedYears.setText(R.string.selectedLength25textView);
                        cal.setYear(Integer.parseInt(((RadioButton) findViewById(year.getCheckedRadioButtonId())).getText().toString()));
                        display();
                        break;
                    case R.id.a30years:
                        selectedYears.setText(R.string.selectedLength30textView);
                        cal.setYear(Integer.parseInt(((RadioButton) findViewById(year.getCheckedRadioButtonId())).getText().toString()));
                        display();
                        break;
                }
            }
        });

    }



    //the listener for amount, only works for onchange status
    private TextWatcher amountWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            try{
                //set the value of amount
                cal.setAmount(Double.parseDouble(charSequence.toString()));
            }catch (NumberFormatException e){
                //catch the exception, e.x. user input .... symbol
                cal.setAmount(0);
            }
            display();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    //the listener for amount, only works for onchange status
    private TextWatcher rateWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            try{
                cal.setRate(Double.parseDouble(charSequence.toString()));
            }catch (NumberFormatException e){
                cal.setRate(0);
            }
            display();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void display(){
        //only display 2 float number
        monthly.setText("$" + String.format("%.02f",cal.getResult()));
    }


}
